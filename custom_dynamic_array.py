#!/usr/bin/env python
# coding: utf-8

# In[1]:


class M(object):
    def public(self):
        print 'Use tab to see me !!'
        
    def _private(self):
        print 'Tab does not work with private function !!'
        


# In[2]:


m = M()


# In[3]:


m.public()


# In[4]:


m._private()


# # Dynamic Array implementation using ctypes library

# In[23]:


import ctypes

class DynamicArray(object):
    
    def __init__(self):
        self.n = 0
        self.capacity = 1
        self.A = self.make_array(self.capacity)
        
    def __len__(self):
        return self.n
    
    def __getitem__(self, index):
        
        if not 0 <= index < self.n:
            return IndexError('array index out of bounds')
        
        return self.A[index]
    
    def append(self, ele):
        
        # check size limits
        if self.n == self.capacity:
            self._resize(2*self.capacity) # Increase capacity by 2x
            
        self.A[self.n] = ele
        self.n += 1
        
    def _resize(self, new_capacity):
        
        # Create higher capacity array
        B = self.make_array(new_capacity)
        
        # Copy elements from old array to new array until n
        for i in range(self.n):
            B[i] = self.A[i]
        
        # Reset old to new
        self.A = B
        self.capacity = new_capacity
    
    def make_array(self, size):
        
        return (ctypes.py_object*size)()    


# In[24]:


d = DynamicArray()


# In[25]:


d.append(30)


# In[26]:


len(d)


# In[35]:


sys.getsizeof(d)


# In[28]:


d.append(50)


# In[29]:


len(d)


# In[31]:


d.append(1)


# In[32]:


len(d)


# In[37]:


for k in range(30):
    d.append(k)
    print sys.getsizeof(d)


# In[38]:


len(d)


# In[39]:


d[25]


# In[41]:


d[1]


# In[42]:


d[100]


# In[ ]:




